<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penyanyi extends Model
{
    protected $table = 'penyanyi';
    protected $fillable = ['nama', 'umur'];

    public $timestamps = false;

    //Relationship one to many penyanyi lagu
    public function lagu() {
        return $this->hasMany('App\Lagu');
    }
}
