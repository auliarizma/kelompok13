<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penilaian extends Model
{
    protected $table = 'penilaian';
    protected $fillable = ['user_id', 'lagu_id', 'rating', 'komentar'];

    public $timestamps = false;

    //Relation many to many penilaian user
    public function user() {
        return $this->belongsTo('App\user');
     }

     //Relation many to many penilaian lagu
    public function lagu() {
        return $this->belongsTo('App\lagu');
     }
}
