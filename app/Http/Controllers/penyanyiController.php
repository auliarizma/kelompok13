<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\genre;
use App\penyanyi;



class penyanyiController extends Controller
{
    public function index()
    {
        $penyanyi= penyanyi::all();
        return view('penyanyi.index', compact('penyanyi'));
    }

   
    public function create()
    {
        $penyanyi=penyanyi::all();
        return view('penyanyi.create',compact('penyanyi'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:penyanyi',
            'umur' => 'required|max:2',
        ]);
       
        $penyanyi= penyanyi::create([
            'nama'=>$request->nama,
            'umur'=>$request->umur
        ]);
        
        return redirect('/penyanyi');
    }

    
    public function show($id)
    {
        $penyanyi=penyanyi::findorfail($id);
        return view('penyanyi.show',compact('penyanyi'));
    }

    
    public function edit($id)
    {
        $penyanyi=penyanyi::all();
        $penyanyi=penyanyi::findorfail($id);
        return view('penyanyi.edit',compact('penyanyi'));
    }

    
    public function update(Request $request, $id)
    {
        $penyanyi=penyanyi::findorfail($id);
        $request->validate([
            'nama'=>'required|unique:genre',
            'umur' => 'required|max:2',
        ]);

        $penyanyi_data = [
            'nama' => $request->nama,
            'umur' => $request->umur
        ];
        $penyanyi -> update($penyanyi_data);

        return redirect('/penyanyi')->with('sukses','nama berhasil diupdate');
    }

    
    public function destroy($id)
    {
        $penyanyi=penyanyi::findorfail($id);
        $penyanyi->delete();
        return redirect('/penyanyi')->with('nama berhasil dihapus');
    }
}
