<?php

namespace App\Http\Controllers;
use App\lagu;
use App\genre;
use App\penyanyi;
use App\user;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $lagu = lagu::count();
        $penyanyi= penyanyi::count();
        $genre= genre::count();
        $user = user::count();
        return view('dashboard.index' ,compact('lagu', 'penyanyi', 'genre', 'user'));
    }
}
