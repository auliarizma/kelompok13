<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\penilaian;
use App\lagu;
use App\User;
//use Auth;

class penilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$penilaian = penilaian::where('user_id', Auth::user()->id)->first();
        $penilaian = penilaian::all();
        return view('penilaian.index', compact('penilaian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$penilaian = penilaian::all();
        $user = User::all();
        $lagu = lagu::all();
        return view('penilaian.create', compact('user', 'lagu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required',
            'lagu_id' => 'required',
            'rating' => 'required',
            'komentar' => 'required'
        ]);
        
        $penilaian=penilaian::create([
            'user_id' => $request->user_id,
            'lagu_id' => $request->lagu_id,
            'rating' => $request->rating,
            'komentar' => $request->komentar
        ]);
        
        return redirect('/penilaian')->with('sukses','penilaian berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penilaian = penilaian::findorfail($id);
        return view('penilaian.show',compact('penilaian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::all();
        $lagu = lagu::all();
        //$penilaian = penilaian::all();
        $penilaian = penilaian::findorfail($id);
        return view('penilaian.edit',compact('user', 'lagu', 'penilaian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penilaian = penilaian::findorfail($id);
        $request->validate([
            'user_id'=>'required',
            'lagu_id' => 'required',
            'rating' => 'required',
            'komentar' => 'required'
        ]);

        $penilaian_data = [
            'user_id'=> $request->user_id,
            'lagu_id' => $request->lagu_id,
            'rating' => $request->rating,
            'komentar' => $request->komentar
        ];

        //penilaian::where($id)->update($penilaian_data);
        $penilaian -> update($penilaian_data);

        return redirect('/penilaian')->with('sukses','penilaian berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penilaian = penilaian::findorfail($id);
        $penilaian->delete();
        return redirect('/penilaian')->with('penilaian berhasil dihapus');
    }
}
