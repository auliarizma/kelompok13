<?php

namespace App\Http\Controllers;
use App\lagu;
use Illuminate\Http\Request;
use PDF;
class PdfController extends Controller
{
 function pdf(){
   $lagu = lagu::all();
 
	$pdf = PDF::loadview('viewpdf',['lagu'=>$lagu]);
	return $pdf->stream();
 }
}
