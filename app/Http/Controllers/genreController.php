<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\genre;
use App\penyanyi;


class genreController extends Controller
{

    public function index()
    {
        $genre= genre::all();
        return view('genre.index', compact('genre'));
    }

   
    public function create()
    {
        $genre=genre::all();
        return view('genre.create',compact('genre'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required|unique:genre',
        ]);
        
        $genre=genre::create([
            'nama'=>$request->nama
        ]);
        
        return redirect('/genre')->with('sukses','genre berhasil dibuat');
        
    }


    public function show($id)
    {
        $genre=genre::findorfail($id);
        return view('genre.show',compact('genre'));
    }

    
    public function edit($id)
    {
        $genre=genre::all();
        $genre=genre::findorfail($id);
        return view('genre.edit',compact('genre'));
    }

    
    public function update(Request $request, $id)
    {
        $genre=genre::findorfail($id);
        $request->validate([
            'nama'=>'required',
        ]);

        $genre_data = [
            'nama' => $request->nama,
        ];
        $genre -> update($genre_data);
        return redirect('/genre')->with('sukses','genre berhasil diupdate');
    }

    
    public function destroy($id)
    {
        $genre=genre::findorfail($id);
        $genre->delete();
        return redirect('/genre')->with('genre berhasil dihapus');
    }
}
