<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lagu;
use App\penilaian;


class ClientviewController extends Controller
{
    public function show($id)
    {
       
        $lagu=lagu::findorfail($id);
        return view('clientview.show',compact('lagu'));
    } 

    public function store(Request $request)
    {
        $request->validate([
           
            'lagu_id'=>'required|unique:penilaian',
            'user_id'=>'required',
            'rating'=>'required',
            'komentar'=>'required'
        ]);
        
        $penilaian=penilaian::create([
            
            'lagu_id'=>$request->lagu_id,
            'user_id'=>$request->user_id,
            'rating'=>$request->rating,
            'komentar'=>$request->komentar
        ]);
        
        return redirect()->route('clientview.show');
        
    }

}
 
