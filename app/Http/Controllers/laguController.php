<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lagu;
use App\genre;
use App\penyanyi;
use File;
use SweetAlert;
class laguController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lagu= lagu::all();
        return view('musik.index', compact('lagu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre= genre::all();
        $penyanyi= penyanyi::all();
        return view('musik.create', compact('genre', 'penyanyi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'genre_id' => 'required',
            'penyanyi_id' => 'required',
            'judul' => 'required|unique:lagu',
            'tahun' => 'required',
            'lirik' => 'required',
            'poster' => 'mimes:jpeg,jpg,png|max:2200',
            'link_lagu' => 'required',
        ]);

        $gambar = $request->poster;
        $new_gambar = time(). '-'.$gambar->getClientOriginalName();

        $lagu = lagu::create([
            'genre_id'  => $request->genre_id,
            'penyanyi_id' => $request->penyanyi_id,
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'lirik' => $request->lirik,
            'poster' => $new_gambar,
            'link_lagu' => $request->link_lagu
        ]);

        $gambar->move('uploads/img/', $new_gambar);
        return redirect('/musik');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lagu=lagu::findorfail($id);
        return view('musik.show',compact('lagu'));
    } 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre=genre::all();
        $penyanyi=penyanyi::all();
        $lagu = lagu::findorfail($id);
        return view('musik.edit', compact( 'genre', 'penyanyi', 'lagu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'genre_id' => 'required',
            'penyanyi_id' => 'required',
            'judul' => 'required',
            'tahun' => 'required',
            'lirik' => 'required',
            'poster' => 'mimes:jpeg,jpg,png|max:2200',
            'link_lagu' => 'required',
        ]);

        $lagu = lagu::findorfail($id);

        if($request->has('poster')) {
            $path = "uploads/img/";
            File::delete($path . $lagu->poster);
            $gambar = $request ->poster;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move("uploads/img", $new_gambar);
            $lagu_data = [

            'genre_id'  => $request->genre_id,
            'penyanyi_id' => $request->penyanyi_id,
            'judul' => $request->judul,
            'tahun' => $request->tahun,
            'lirik' => $request->lirik,
            'poster' => $new_gambar,
            'link_lagu' => $request->link_lagu
                
        ];
            } else {
            $lagu_data = [
                'genre_id'  => $request->genre_id,
                'penyanyi_id' => $request->penyanyi_id,
                'judul' => $request->judul,
                'tahun' => $request->tahun,
                'lirik' => $request->lirik,
                'link_lagu' => $request->link_lagu
            ];
        }
        $lagu -> update($lagu_data);
        return redirect()->route('musik.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lagu = lagu::findorfail($id);
        $lagu->delete();

        $path = "uploads/img/";
        File::delete($path . $lagu->poster);

        return redirect()->route('musik.index');
    }
}
