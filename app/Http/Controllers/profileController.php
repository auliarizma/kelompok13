<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\profile;
use Auth;

class profileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$profile = profile::all();
        $profile = profile::where('user_id', Auth::user()->id)->first();
        //$profile = profile::where('user_id', user()->id)->first();
        return view('profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$profile = profile::all();
        //return view('profile.create', compact('profile'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required|unique:profile',
            'bio' => 'required',
            'alamat' => 'required',
            //'user_id' => 'required'
        ]);
        
        $profile = profile::create([
            'nama' => $request->nama,
            'bio' => $request->bio,
            'alamat' => $request->alamat,
            'user_id' => Auth::user()->id
        ]);
        
        return redirect('/profile')->with('sukses','profile berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$profile = profile::findorfail($id);
        //return view('profile.show',compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$profile = profile::all();
        //$profile = profile::findorfail($id);
        //return view('profile.edit',compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = profile::findorfail($id);
        $request->validate([
            'nama'=>'required|unique:profile',
            'bio' => 'required',
            'alamat' => 'required',
            'user_id' => 'required'
        ]);

        return redirect('/profile')->with('sukses','profile berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$profile = profile::findorfail($id);
        //$profile->delete();
        //return redirect('/profile')->with('profile berhasil dihapus');
    }
}
