<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['nama', 'bio', 'alamat', 'user_id'];

    public $timestamps = false;

    //Relation one to one user profile
    public function user() {
       return $this->belongsTo('App\User');
    }
}
