<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    protected $table = 'genre';
    protected $fillable = ['nama'];

    public $timestamps = false;

    public function lagu()
    {
     return $this->hasMany('App\lagu');
    }

}
