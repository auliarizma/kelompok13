<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lagu extends Model
{
    protected $table ='lagu';
    protected $fillable = ['genre_id', 'penyanyi_id', 'judul', 'tahun', 'lirik', 'poster', 'link_lagu'];

   public $timestamps = false;

    public function genre()
    {
        return $this->belongsTo('App\genre');
    }
    public function penyanyi()
    {
        return $this->belongsTo('App\penyanyi');
    }

    //Relationship many to many (penilaian)
    public function penilaian() {
        return $this->hasMany('App\penilaian');
    }
}
