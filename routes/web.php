<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/','HomeController@index')->name('home');
Route::get('/clientview/{Id}', 'ClientviewController@show');
Route::get('/viewgenre/index{Id}', 'liatgenreController@index');
Route::get('/viewgenre/{Id}', 'liatgenreController@show');
Route::post('/clientview', 'ClientviewController@store');
//Route::get('/client', function () {
    //return view('client');
//}); 

//Middleware
Route::group(['middleware' => ['auth']], function(){
    Route::get('/dashboard', 'AdminController@index');
    Route::resource('musik', 'laguController');
    
    Route::resource('genre','genreController');
    Route::resource('penyanyi','penyanyiController');
    
    Route::resource('profile','profileController')->only(['index', 'update', 'store']);
    Route::resource('penilaian','penilaianController');
    Route::get('viewpdf', 'PdfController@pdf');
    //route::get('/dashboard', function(){
       // return view('dashboard.index');
   // });
});

route::get('/biodata', function(){
    return view('clientview.biodata');
});
route::get('/about', function(){
    return view('clientview.about');
});
//Route::get('/home', 'HomeController@index')->name('home');
//Route::group(['middleware' => ['auth','checkRole:admin']],function(){ 
//Route::group(['middleware' => ['auth','checkRole:admin']],function(){ 
