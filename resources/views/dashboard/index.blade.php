@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Halaman Dasboard
@endsection
@push('scripts')
<script>
    Swal.fire({
        title: "Login Berhasil!",
        
        icon: "success",
        confirmButtonText: "Tutup",
    });
</script>
@endpush
@section('content')


                  <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <br><br>
                    <div style="text-align: center">
                  <!-- Page Heading -->
                  <div class="align-items-center  mb-4" >
                      <h1 class="h3 mb-0 text-gray-800" >Welcome <b style="color: blue">{{Auth::user()->name}}</b> di Halaman Admin Musik</h1>
                  </div>
                    </div>

                  <div class="row">

                    
                      <!-- Earnings (Annual) Card Example -->
                      <div class="col-xl-3 col-md-6 mb-4">
                          <div class="card border-left-success shadow h-100 py-2">
                              <div class="card-body">
                                  <div class="row no-gutters align-items-center">
                                      <div class="col mr-2">
                                          <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                              Lagu</div>
                                          <div class="h5 mb-0 font-weight-bold text-gray-800">Jumlah Data <span class="badge badge-pill badge-success">{{$lagu}}</span></div>
                                      </div>
                                      <div class="col-auto">
                                          <i class=" fas fa-music fa-2x text-gray-300"></i>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- Pending Requests Card Example -->
                      <div class="col-xl-3 col-md-6 mb-4">
                          <div class="card border-left-warning shadow h-100 py-2">
                              <div class="card-body">
                                  <div class="row no-gutters align-items-center">
                                      <div class="col mr-2">
                                          <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                             Genre</div>
                                          <div class="h5 mb-0 font-weight-bold text-gray-800">Jumlah Data <span class="badge badge-pill badge-warning">{{$genre}}</span></div>
                                      </div>
                                      <div class="col-auto">
                                          <i class="fas fa-venus-mars fa-2x text-gray-300"></i>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Penyanyi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">Jumlah Data <span class="badge badge-pill badge-primary">{{$penyanyi}}</span></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class=" fas fa-microphone fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                      <div class="card border-left-danger shadow h-100 py-2">
                          <div class="card-body">
                              <div class="row no-gutters align-items-center">
                                  <div class="col mr-2">
                                      <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                          User</div>
                                      <div class="h5 mb-0 font-weight-bold text-gray-800">Jumlah Data <span class="badge badge-pill badge-danger">{{$user}}</span></div>
                                  </div>
                                  <div class="col-auto">
                                      <i class=" fas fa-user fa-2x text-gray-300"></i>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  </div>
                  
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
@endsection