@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Halaman Profile
@endsection
@push('scripts')
<script src="https://cdn.tiny.cloud/1/vlvjp793t38nxv7m02lnp581kjw6rpq8zjpwk4onscsvby07/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      forced_root_block : ""
   });
  </script>
  @endpush
@section('content') 
<div>
        <form action="/profile" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <input type="text" class="form-control" name="bio" placeholder="bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Alamat</label><br>
                <textarea name="alamat" cols="134" rows="5"></textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">User</label>
                <select name="user_id" class="form-control">
                    <option value=""> -- Pilih User -- </option>
                    @foreach ($profile as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>   
                    @endforeach
                </select>
                @error('user_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection