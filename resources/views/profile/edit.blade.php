@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Halaman Profile
@endsection
@push('scripts')
<script src="https://cdn.tiny.cloud/1/vlvjp793t38nxv7m02lnp581kjw6rpq8zjpwk4onscsvby07/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      forced_root_block : ""
   });
  </script>
  @endpush
@section('content') 
<div>
    <form action="/profile/{{$profile->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="nama" value="{{$profile->nama}}">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            <label>Bio</label>
            <input type="integer" class="form-control" name="bio" placeholder="bio" value="{{$profile->bio}}">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            <label>Alamat</label><br>
            <textarea name="alamat" cols="134" rows="5">{{$profile->alamat}}</textarea>
            @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        <br><button type="submit" class="btn btn-primary">Simpan</button>
    </form>
</div>
@endsection