@extends('admin-layouts.master')

@section('title')
Home Admin
@endsection

@section('title')
    Halaman Profile
@endsection

@section('content') 

<!--Relasi profile-->

<!--Jika profile tidak kosong maka akan update data-->

@if ($profile != null)

<div class="container-fluid-flex">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4 class="card-title">Data <span class="badge badge-secondary">Profile</span></h4>
        </div>
        <form action="/profile" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="card-body">
            <p style="line-height: 10px"><b>Nama :</b> {{$profile->user->name}}</p>
            <p style="line-height: 10px"><b>Email :</b> {{$profile->user->email}}</p>
            <br>
            <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="nama" placeholder="nama" value="{{$profile->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Bio</label>
                <textarea name="alamat" rows="2" class="form-control">{{$profile->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Alamat</label><br>
                <textarea name="alamat" rows="4" class="form-control">{{$profile->alamat}}</textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </form>
</div>

@else

<!--Jika profile kosong maka akan create data-->
<div class="container-fluid-flex">
    <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h4 class="card-title">Masukkan Data <span class="badge badge-secondary">Profile</span></h4>
            </div>
            <form action="/profile" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan nama profile / username">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                </div>
                <div class="form-group">
                    <label>Bio</label>
                    <textarea name="bio" rows="2" class="form-control" placeholder="Masukkan bio"></textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Alamat</label><br>
                    <textarea name="alamat" rows="4" class="form-control" placeholder="Masukkan alamat"></textarea>
                    @error('alamat')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
        </form>
    </div>
</div>

    
@endif

@endsection