<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Kelompok 13 Laravel Sanbercode</h4>
		<h6><a>Laporan Data Musik</a></h5>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th>No</th>
                <th>Genre</th>
                <th>Penyanyi</th>
                <th>Umur</th>
                <th>Judul</th>
                <th>Tahun</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($lagu as $p)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$p->genre->nama}}</td>
				<td>{{$p->penyanyi->nama}}</td>
				<td>{{$p->penyanyi->umur}}</td>
				<td>{{$p->judul}}</td>
				<td>{{$p->tahun}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>