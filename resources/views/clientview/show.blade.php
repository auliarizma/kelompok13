@extends('client-layouts.master')
@section('title')
SolMusic | Detail
@endsection


@section('content')
    
	<!-- Player section -->
	<section class="player-section set-bg" data-setbg="{{ asset('/client/img/player-bg.jpg')}}">
		<div class="player-box">
			<div class="tarck-thumb-warp">
				<div class="tarck-thumb">
					<img src="{{asset('/uploads/img/' . $lagu->poster)}}" alt="">
					<button onclick="wavesurfer.playPause();" class="wp-play"></button>
				</div>
			</div>
			<div class="wave-player-warp">
				<div class="row">
					<div class="col-lg-8">
						<div class="wave-player-info">
							<h2>{{$lagu->judul}}</h2>
							
						</div>
					</div>
					<div class="col-lg-4">
						<div class="songs-links">
							<a href="link_lagu" download ><img src="{{ asset('/client/img/icons/p-2.png')}}" alt=""></a>
						</div>
					</div>
				</div>
				<div id="wavePlayer" class="clierfix">
					<div id="audiowave" data-waveurl="{{$lagu->link_lagu}}"></div>
					<div id="currentTime"></div>
					<div id="clipTime"></div>
					<!-- Player Controls -->
					<div class="wavePlayer_controls">
						<button class="jp-prev player_button" onclick="wavesurfer.skipBackward();"></button>
						<button class="jp-play player_button" onclick="wavesurfer.playPause();"></button>
						<button class="jp-next player_button" onclick="wavesurfer.skipForward();"></button>
						<button class="jp-stop player_button" onclick="wavesurfer.stop();"></button>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Player section end -->

	<!-- Songs details section -->
	<section class="songs-details-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3">
					<div class="song-details-box">
						<h3 style="text-align: center">Lirik Lagu</h3>
						<ul>
							<li><strong>Lirik:</strong><br>
							<span><pre>{{$lagu->lirik}}</pre></span></li>
							
						</ul>
					</div>
				</div>
				<div class="col-lg-9">
					<div class="row">
						<div class="col-lg-6">
							<div class="song-details-box">
								<h3 style="text-align: center">Tentang Artis</h3>
								<div class="artist-details">
									<img src="{{asset('/uploads/img/' . $lagu->poster)}}" alt="">
									<div class="ad-text">
										<h5>{{$lagu->penyanyi->nama}}</h5>
										<span>Umur :{{$lagu->penyanyi->umur}}</span>
										<p>Genre: {{$lagu->genre->nama}} </p>
										<p>Judul Lagu: {{$lagu->judul}} </p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="song-details-box" style="line-spacing:10px">
								<h3 style="text-align: center">Rate</h3>
								<!-- song -->
								@foreach ($lagu->penilaian as $item)

								<ul>
									<li><span style="color: blue"><b>{{$item->user->name}} </b></span>
										<p class="mb-0"><b>Rating :</b> {{$item->rating}} / 10</p>
										<p><b>Komentar :</b> {{$item->komentar}} </p>
										</ul>
									</li>
								</ul>
								
								@endforeach
								<!-- song -->
								<div class="song-item">
									<div class="row">
										<div class="col-xl-5 col-lg-12 col-md-5">
											
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Songs details section -->
	
	<!-- Similar Songs section -->
	<section class="similar-songs-section">
		<div class="container-fluid">
			<h3>Beri Rating</h3>
			<div class="row">
				<div class="col-xl-3 col-sm-6">
			    
					<button class="btn btn-outline-dark" id="btn-komentar-utama"><i class="far fa-comment"></i> Beri Komentar</button>
					<form action="{{ url('clientview')}}" style="margin-top:20px; display:none;" id="komentar-utama" method="POST">
						@csrf
						<input type="hidden" name="lagu_id" value="{{$lagu->id}}">
					<input type="hidden" name="user_id" value="1">
					<input type="hidden" name="rating" value="5">
				<textarea  name="komentar" class="form-control mt-3 mb-sm-1"  rows="4px"></textarea>
                
				 <input type="submit" class="btn btn-primary" value="kirim">
				</form>
				
				</div>	
				</div>
				
				
			</div>
		</div>
			</div>
		</div>
	</section>
	<!-- Similar Songs section end -->
@endsection

@push('scripts')
    <!-- Audio Players js -->
	<script src="{{ asset('/client/js/jquery.jplayer.min.js')}}"></script>
	<script src="{{ asset('/client/js/wavesurfer.min.js')}}"></script>

	<!-- Audio Players Initialization -->
	<script src="{{ asset('/client/js/WaveSurferInit.js')}}"></script>
	<script src="{{ asset('client/js/jplayerInit.js')}}"></script>
	<script>
		$(document).ready(function(){
$('#btn-komentar-utama').click(function(){
	$('#komentar-utama').toggle('slide');
});
		});


	</script>
@endpush