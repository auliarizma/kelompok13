@extends('client-layouts.master')
@section('title')
SolMusic | About
@endsection

@section('content')
<div style="background-color: #0a183d" >
  <br><br>
  <div class="container-fluid">
  <div class="card text-center ml-5 mr-5">
    <div class="card-header">
      Final Project #Laravel Web Development
    </div>
    <div class="card-body">
      <h5 class="card-title">Kelompok 13</h5>
      <p class="card-text"> Web ini kami buat untuk mengaplikasikan materi-materi pembelajaran selama mengikuti Bootcamp Web Developer Sanbercode.</p>
      <a href="#" class="btn btn-primary">Ary Rizma Aulia</a>
      <a href="#" class="btn btn-secondary">Novia Arifa Ningsih</a>
      <a href="#" class="btn btn-success">Ulfa Hafiza</a>
      <p><p><a href="/biodata" class="btn btn-warning">Biodata</a>
      </p></p>
    </div>
    <div class="card-footer text-muted">
      Indonesia Mengoding SanberCode Batch 25
    </div>
  </div>
</div>
</br></br>
</div>

@endsection