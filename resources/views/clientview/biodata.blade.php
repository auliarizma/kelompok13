@extends('client-layouts.master')
@section('title')
SolMusic | Biodata
@endsection

@section('content')

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

  </head>
  <body style="background-color: #0a183d">
<!-- KOLOM 1! -->
<div class="d-flex justify-content-center bg-light mt-5 mr-5 ml-5" style="border-radius: 20px">
  <div class="container"><br><br>
  <h1 style="text-align: center">Biodata</h1><br>
    <div class="row" style="text-align: center">
      <div class="col-4">
          <img src="/uploads/img/8ae1d239-ca60-4fe6-9a19-503897d76f99.jpg" class="card-img-top w-100 h-50 " alt="...">
          <p></p>
          <h3>Ary Aulia Rizma</h3>
          <p>Kuningan</p>
        </div>
  
  <!-- KOLOM 2! -->
      <div class="col-4">
          <img src="/uploads/img/as35arw4rasdfw4.jpeg" class="card-img-top w-100 h-50 " alt="...">
          <p></p>
          <h3>Novia Arifa Ningsih</h3>
          <p>Depok</p>
        </div>
  
  
  
  <!-- KOLOM3! -->
  
   <div class="col-4">
   <img src="/uploads/img/9c7424cd-e91d-4e9f-acdc-47c3f1d901ff.jpg " class="card-img-top w-100 h-50 " alt="...">
        <p></p>  
        <h3>Ulfa Hafiza</h3>
          <p>Cimahi</p>
        </div>
    </div>
  </div>
  </div>
</div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>

@endsection
