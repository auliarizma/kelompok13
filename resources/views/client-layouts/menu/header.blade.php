<header class="header-section clearfix">
    <a href="/" class="site-logo">
        <img src="{{ asset('/client/img/logo.png')}}" alt="">
    </a>
    <div class="header-right">
        <div class="user-panel">
            <a href="/login" class="login">Login</a>
            <a href="/register" class="register">Create an account</a>
        </div> 
    </div>
    <ul class="main-menu">
        <li><a href="/">Home</a></li>
        <li><a href="/about">About</a></li>
        <li><a href="#playlist">Playlist</a></li>
        <li><a href="https://sanbercode.com/#paket">Contact</a></li>
        
    </ul>
</header>

