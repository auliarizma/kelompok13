<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>@yield('title')</title>
	<meta charset="UTF-8">
	<meta name="description" content="SolMusic HTML Template">
	<meta name="keywords" content="music, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Favicon -->
	<link href="{{ asset('/client/img/favicon.ico')}}" rel="shortcut icon"/>
	<link href="https://kit-pro.fontawesome.com/releases/v5.15.3/css/pro.min.css" rel="stylesheet">
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">
 
	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{ asset('/client/css/bootstrap.min.css')}}"/>
	<link rel="stylesheet" href="{{ asset('/client/css/font-awesome.min.css')}}"/>
	<link rel="stylesheet" href="{{ asset('/client/css/owl.carousel.min.css')}}"/>
	<link rel="stylesheet" href="{{ asset('/client/css/slicknav.min.css')}}"/>

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="{{ asset('/client/css/style.css')}}"/>


	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>
<body>
	<!-- Page Preloder -->
	

	<!-- Header section -->
	@include('client-layouts.menu.header')
	<!-- Header section end -->

	<!-- Blog section -->
	
		@yield('content')


			
		
	<!-- Blog section end -->

	<!-- Footer section -->
	<footer class="footer-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-7 order-lg-2">
					<div class="row">
						<div class="col-sm-4">
							<div class="footer-widget">
								<h2>About us</h2>
								<ul>
									<li><a href="/biodata">Biodata</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="footer-widget">
								<h2>Products</h2>
								<ul>
									<li><a href="/musik">Music</a></li>

								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-5 order-lg-1">
					<img src="{{ asset('/client/img/logo.png')}}" alt="">
					<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
					<div class="social-links">
						<a href="https://www.instagram.com/sanbercode/"><i class="fa fa-instagram"></i></a>
						<a href="https://web.facebook.com/sanbercode?_rdc=1&_rdr"><i class="fa fa-facebook"></i></a>
						<a href="https://www.youtube.com/channel/UC_AbGHyMz2e-mxkS8UvETvA"><i class="fa fa-youtube"></i></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end -->
	
	<!--====== Javascripts & Jquery ======-->
	<script src="{{ asset('/client/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{ asset('/client/js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('/client/js/jquery.slicknav.min.js')}}"></script>
	<script src="{{ asset('/client/js/owl.carousel.min.js')}}"></script>
	<script src="{{ asset('/client/js/mixitup.min.js')}}"></script>
	<script src="{{ asset('/client/js/main.js')}}"></script>
	@stack('scripts')
	</body>
</html>
