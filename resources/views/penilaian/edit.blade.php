@extends('admin-layouts.master')
@section('title')
Home Admin | Penilaian
@endsection
@push('scripts')
<script src="https://cdn.tiny.cloud/1/vlvjp793t38nxv7m02lnp581kjw6rpq8zjpwk4onscsvby07/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      forced_root_block : ""
   });
  </script>
  @endpush
@section('content') 
<div class="mr-4 ml-4 mt-3">
    <div class="card ">
        <div class="card-header">
            <h3 class="card-title">Edit Data Penilaian</h3>
        </div>
        <form action="/penilaian/{{$penilaian->id}}" enctype="multipart/form-data" method="POST">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label for="body">User</label>
                    <select name="user_id" class="form-control" id="exampleFormControlSelect1">
                        <option value=""> --- Pilih User --- </option>
                        @foreach ($user as $item)
                        @if ($item->id === $penilaian->user_id)
                            <option value="{{$item->id}}" selected>{{$item->name}}</option>   
                            @else
                        <option value="{{ $item->id}}">{{$item->name}}</option>
                        @endif
                        @endforeach
                    </select>

                    @error('user_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Lagu</label>
                    <select name="lagu_id" class="form-control">
                        <option value=""> --- Pilih Lagu --- </option>
                        @foreach ($lagu as $item)
                        @if ($item->id === $penilaian->lagu_id)
                            <option value="{{$item->id}}" selected>{{$item->judul}}</option>   
                            @else
                        <option value="{{ $item->id}}">{{$item->judul}}</option>
                        @endif
                        @endforeach
                    </select>
                    @error('lagu_id')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Rating</label>
                    <input type="integer" class="form-control" name="rating" placeholder="rating" value="{{$penilaian->rating}}">
                    @error('rating')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Komentar</label><br>
                    <textarea name="komentar" cols="134" rows="5">{{$penilaian->komentar}}</textarea>
                    @error('komentar')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan Data</button>
            </div>
        </form>
       
    </div>
</div>
@endsection