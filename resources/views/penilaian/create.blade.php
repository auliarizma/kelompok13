@extends('admin-layouts.master')

@section('title')
Home Admin
@endsection

@section('title')
    Halaman Penilaian
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/vlvjp793t38nxv7m02lnp581kjw6rpq8zjpwk4onscsvby07/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      forced_root_block : ""
   });
  </script>
  @endpush

@section('content') 
<div>
    <form action="/penilaian" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="form-group">
            <select name="user_id" class="form-control" id="exampleFormControlSelect1">
                <option value=""> -- Pilih User -- </option>
                @foreach ($user as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>   
                @endforeach
            </select>
            @error('user_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Lagu</label>
            <select name="lagu_id" class="form-control">
                <option value=""> -- Pilih Lagu -- </option>
                @foreach ($lagu as $item)
                    <option value="{{$item->id}}">{{$item->judul}}</option>   
                @endforeach
            </select>
            @error('lagu_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Rating</label>
            <input type="integer" class="form-control" name="rating" placeholder="Beri Rating dari 1 - 10">
            @error('rating')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Komentar</label><br>
            <textarea name="komentar" cols="132" rows="5"></textarea>
            @error('komentar')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <br><button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>

  @endsection