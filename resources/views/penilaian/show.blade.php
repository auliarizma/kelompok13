@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Halaman Penilaian
@endsection
@section('content')

<div class="card" style="line-height:10px">
    <h5 class="card-header">Nama : {{$penilaian->user->name}}</h5>
    <div class="card-body">
      <h4 class="card-text">Judul Lagu : {{$penilaian->lagu->judul}}</h4>
      <p>Rating : {{$penilaian->rating}}</p>
      <p>Komentar : {{$penilaian->komentar}}</p>
      
    </div>
    <div class="card-footer">
        <a href="/penilaian" class="btn btn-primary">Kembali</a>
    </div>
</div>

@endsection