@extends('admin-layouts.master')

@section('title')
Home Admin
@endsection

@section('title')
    Halaman Penyanyi
@endsection

@push('link')
<link href="{{ asset('/admin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush

@section('content') 
<div class="container-fluid-flex">
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h4 class="card-title">Data <span class="badge badge-secondary">Penilaian</span></h4><Br>
            <a href="/penilaian/create" type="button" class="btn btn-outline-primary mb-2">Tambah Data</a>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align: center">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">User</th>
                        <th scope="col">Lagu</th>
                        <th scope="col">Rating</th>
                        <th scope="col">Komentar</th>
                        <th scope="col">Opsi</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">User</th>
                        <th scope="col">Lagu</th>
                        <th scope="col">Rating</th>
                        <th scope="col">Komentar</th>
                        <th scope="col">Opsi</th>
                    </tr>
                </tfoot>
            <tbody>
                @forelse ($penilaian as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$value->user->name}}</td>
                        <td>{{$value->lagu->judul}}</td>
                        <td>{{$value->rating}}</td>
                        <td style="text-align: left">{{$value->komentar}}</td>
                      
                        <td style="display: flex;">
                        <form action="/penilaian/{{$value->id}}" method="POST" enctype="multipart/form-data">
                            <a href="/penilaian/{{$value->id}}" ><i class="fa fa-camera-retro fa-2x mr-lg-3"></i></a> ||
                            <a href="/penilaian/{{$value->id}}/edit" ><i class="fa fa-pencil-alt fa-2x ml-lg-3"></i></a> ||
                            @csrf 
                            @method('DELETE')
                            <input type="submit" class="btn btn-outline-primary ml-lg-3" value="Delete">
                        </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                      <td colspan="4" align="center">Belum Ada Data</td>
                    </tr>
                @endforelse 
                           
            </tbody>
        </table>
        </div>
    </div></div>
</div>
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{ asset('/admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('/admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('/admin/js/demo/datatables-demo.js')}}"></script>
@endpush