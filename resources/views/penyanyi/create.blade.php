@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Halaman Penyanyi
@endsection
@section('content') 
<div>
        <form action="/penyanyi" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Penyanyi"><br>
                <label>Umur</label>
                <input type="integer" class="form-control" name="umur"  placeholder="Masukkan Umur Penyanyi">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
    
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <br><button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection