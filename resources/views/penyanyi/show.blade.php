@extends('admin-layouts.master')
@section('title')
    Home Admin | Penyanyi
@endsection

@section('content')

<div class="card">
    <h5 class="card-header">Nama : {{$penyanyi->nama}}</h5>
    <div class="card-body">
      <p class="card-text">Umur : {{$penyanyi->umur}}</p>
      
    </div>
    <div class="card-footer">
      <a href="/penyanyi" class="btn btn-primary">Kembali</a>
    </div>
  </div>

@endsection