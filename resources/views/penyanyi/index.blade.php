@extends('admin-layouts.master')

@section('title')
    Halaman Index Penyanyi
@endsection

@push('link')
<link href="{{ asset('/admin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush
@section('content')



<div class="container-fluid-flex">

    <!-- Page Heading -->
   
    

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4 class="card-title">Data <span class="badge badge-secondary">Penyanyi</span></h4><Br>
                <a href="/penyanyi/create" type="button" class="btn btn-outline-primary mb-2">Tambah Data</a>
                
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align: center">
                    <thead >
                        <tr>
                            <th>No</th>
                            <th>Nama penyanyi</th>
                            <th>Umur</th>
                            <th>List Lagu</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tfoot >
                       
                        <tr>
                            <th>No</th>
                            <th>Nama penyanyi</th>
                           <th>Umur</th>
                            <th>List Lagu</th>
                            <th>Opsi</th>
                        </tr>
                    </tfoot>
                    <tbody> 
                        @forelse ($penyanyi as $key=>$value)
                        <tr>
                            <td>{{$key +1}}</td>
                            <td>{{$value->nama}}</td>
                            <td>{{$value->umur}}</td>
                            <td style="text-align: left">
                                <ul>
                                    @foreach ($value->lagu as $item)
                                        <li>{{$item->judul}}</li>
                                    @endforeach
                                </ul>
                            </td>
    
                            
                            <td style="display: flex;">
                               
                            <form action="/penyanyi/{{$value->id}}" method="POST"  enctype="multipart/form-data">
                                @csrf 
                                @method('DELETE')
                                <a href="/penyanyi/{{$value->id}}" ><i class="fa fa-camera-retro fa-2x mr-lg-3"></i></a> || 
                                <a href="/penyanyi/{{$value->id}}/edit" ><i class="fa fa-pencil-alt fa-2x ml-lg-3"></i></a>  ||  
                                <input type="submit" class="btn btn-outline-primary ml-lg-3" value="Delete">
                            </form>
                            </td>
                            
                        </tr>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="4" align="center">Belum Ada Data</td>
                  </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{ asset('/admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('/admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('/admin/js/demo/datatables-demo.js')}}"></script>
@endpush