@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Halaman Penyanyi
@endsection
@section('content') 
<div class="mr-4 ml-4 mt-3">
    <div class="card ">
        <div class="card-header">
            <h3 class="card-title">Edit Data Penyanyi</h3>
        </div>
        <form role="form" action="/penyanyi/{{$penyanyi->id}}" enctype="multipart/form-data" method="POST">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{$penyanyi->nama}}" placeholder="nama"><br>
                    <label>Umur</label>
                    <input type="integer" class="form-control" name="umur" value="{{$penyanyi->umur}}" placeholder="umur">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
        
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan Data</button>
            </div>
        </form>
       
    </div>
</div>

@endsection