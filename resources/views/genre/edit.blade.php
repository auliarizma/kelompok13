@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Halaman Genre
@endsection
@section('content')  
<div class="mr-4 ml-4 mt-3">
    <div class="card ">
        <div class="card-header">
            <h3 class="card-title">Edit Data Genre</h3>
        </div>
        <form action="/genre/{{$genre->id}}" enctype="multipart/form-data" method="POST">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label>Nama genre</label>
                    <input type="text" class="form-control" name="nama" value="{{$genre->nama}}" placeholder="nama">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan Data</button>
            </div>
        </form>
    </div>
</div>
@endsection