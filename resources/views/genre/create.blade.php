@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Halaman Genre
@endsection
@section('content')  
<div>
        <form action="/genre" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama genre</label>
                <input type="text" class="form-control" name="nama" placeholder="nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <br><button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection