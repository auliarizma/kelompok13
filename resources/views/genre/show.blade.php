@extends('admin-layouts.master')
@section('title')
Home Admin | Genre
@endsection
@section('content')
<div class="card">
    <h5 class="card-header">Genre : {{$genre->nama}}</h5>
    <div class="card-body">
      <a href="/genre" class="btn btn-primary">Kembali</a>
    </div>
  </div>

@endsection