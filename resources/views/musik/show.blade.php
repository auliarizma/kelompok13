@extends('admin-layouts.master')
@section('title')
Home Admin
@endsection
@section('title')
    Show Lagu
@endsection
@section('content')
<div class="row">
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <br>
            <h2 style="text-align: center; color:black; font-weight:bold">{{$lagu->penyanyi->nama}}</h2>
            <img class="card-img-top" width="300px" height="250px" src="{{asset('/uploads/img/' . $lagu->poster)}}" alt="Card image cap">
            <div class="card-body">
            <h5 class="card-title">{{$lagu->judul}} ({{$lagu->tahun}}) <span class="badge badge-success">{{$lagu->genre->nama}}</span></h5>
            
              <p class="card-text"><pre>{{$lagu->lirik}}</pre></p>
            </div>
        </div>
    </div>
    
     <div class="col-8">
         <h4 style="font-weight: bold">Penilaian dan Komentar : </h4>
         <ul class="list-group">
             @foreach ($lagu->penilaian as $item)
                 <li class="list-group-item"><h5 style="color: green">{{$item->user->name}}</h5>  <p>Rating : {{$item->rating}} / 10 , Komentar : {{$item->komentar}}</p></li>
             @endforeach
         </ul>
         <br>
         <a href="/musik" class="btn btn-primary">Kembali</a>
     </div>
     
</div>


@endsection