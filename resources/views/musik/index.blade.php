@extends('admin-layouts.master')

@section('title')
    Halaman Index Musik
@endsection

@push('link')
<link href="{{ asset('/admin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush
@section('content')



<div class="container-fluid-flex">

    <!-- Page Heading -->
   
    

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4 class="card-title">Data <span class="badge badge-secondary">Lagu</span></h4><Br>
                <a href="/musik/create" type="button" class="btn btn-outline-primary mb-2">Tambah Data</a>
                <a href="/viewpdf" type="button" class="btn btn-outline-warning mb-2">Cetak</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align: center">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>Genre</th>
                          <th>Penyanyi</th>
                          <th>Judul</th>
                          <th>Tahun</th>
                          <th>Lirik</th>
                         <th>Poster</th> 
                        
                         <th>Opsi</th>
                            
                            
                        </tr>
                    </thead>
                    <tfoot>
                       
                        <tr>
                          <th>No</th>
                          <th>Genre</th>
                          <th>Penyanyi</th>
                          <th>Judul</th>
                          <th>Tahun</th>
                          <th>Lirik</th>
                         <th>Poster</th> 
                         
                         <th>Opsi</th>
                           
                        </tr>
                    </tfoot>
                    <tbody> 
                        @forelse ($lagu as $key=>$value)
                            <tr>
                            <td>{{$key +1}}</td>
                            <td>{{$value->genre->nama}}</td>
                            <td>{{$value->penyanyi->nama}}</td>
                            <td>{{$value->judul}}</td>
                            <td>{{$value->tahun}}</td>
                            <td style="text-align:left">{{Str::limit($value->lirik, 100)}}</td>
                            <td><img class="img-thumbnail" width="40px" height="25" src="{{asset('/uploads/img/' . $value->poster)}}" ></td>
                            
                            
                            
                            <td style="display: flex;">
                                <a href="/musik/{{$value->id}}" ><i class="fa fa-camera-retro fa-2x mr-lg-3"></i></a>||
                                <a href="/musik/{{$value->id}}/edit" ><i class="fa fa-pencil-alt fa-2x ml-lg-3"></i></a> || 
                            <form action="/musik/{{$value->id}}" method="POST"  enctype="multipart/form-data">
                                @csrf 
                                @method('DELETE')
                                
                                <input type="submit" class="btn btn-outline-primary ml-lg-3" value="Delete">
                            </form>
                            </td>
                            
                        </tr>
                        @empty
                        <tr>
                          <td colspan="4" align="center">Belum Ada Data</td>
                      </tr>
                        @endforelse
                        
                        
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{ asset('/admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('/admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('/admin/js/demo/datatables-demo.js')}}"></script>
@endpush
