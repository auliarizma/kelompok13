@section('title')
    Halaman Tambah Data Lagu
@endsection
@extends('admin-layouts.master')

@section('content')
<div class="mr-4 ml-4 mt-3">
    <div class="card ">
        <div class="card-header">
            <h3 class="card-title">Edit Data Lagu</h3>
        </div>

        <form role="form" action="/musik/{{$lagu->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}

            <div class="card-body">
              <div class="form-group">
                    <label for="exampleFormControlSelect1">Pilih Genre</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="genre_id">
                      <option value="">--- Pilih Genre ---</option>
                      @foreach ($genre as $item)
                      @if ($item->id === $lagu->genre_id)
                      <option value="{{ $item->id}}" selected>{{$item->nama}}</option>
                      @else
                      <option value="{{ $item->id}}">{{$item->nama}}</option>
                      @endif
                      @endforeach
                    </select><br>
                    @error('genre_id')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>


                  
                    <div class="form-group">
                          <label for="exampleFormControlSelect1">Pilih Penyanyi</label>
                          <select class="form-control" id="exampleFormControlSelect1" name="penyanyi_id">
                            <option value="">--- Pilih Penyanyi ---</option>
                            @foreach ($penyanyi as $item)
                            @if ($item->id === $lagu->penyanyi_id)
                            <option value="{{ $item->id}}" selected>{{$item->nama}}</option>
                            @else
                            <option value="{{ $item->id}}">{{$item->nama}}</option>
                            @endif
                            @endforeach
                          </select><br>
                          @error('genre_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                        </div>

                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul"  name="judul" value="{{ old('judul', $lagu->judul) }}" placeholder="Masukan Judul"><br>
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="text" class="form-control" id="tahun" name="tahun" value="{{ old('tahun', $lagu->tahun) }}" placeholder="Masukan Tahun"><br>
                    @error('tahun')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                
                    
                <div class="form-group">
                        <label for="lirik">Lirik</label>
                        <textarea class="form-control" id="" name="lirik" rows="4">{{ old('lirik', $lagu->lirik) }}</textarea><br>
                        @error('lirik')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>  
                  
                    <div class="form-group">
                      <label>Upload Poster</label>
                      <input type="file" class="form-control-file" name="poster" ><br>
                      @error('poster')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    </div>
                    
                  <div class="form-group">
                        <label for="link_lagu">Link Lagu</label>
                        <input type="text" class="form-control" id="link_lagu"  name="link_lagu" value="{{ old('link_lagu', $lagu->link_lagu) }}" placeholder="Masukan Link"><br>
                        @error('link_lagu')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                

                                      
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan Data</button>
            </div>
            
    </div>
</div>
@endsection

