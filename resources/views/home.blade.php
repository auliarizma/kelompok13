@extends('client-layouts.master')
@section('title')
SolMusic | Home Client
@endsection


@section('content')

<!-- Hero section -->
<section class="hero-section">
    <div class="hero-slider owl-carousel">
        <div class="hs-item">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hs-text">
                            <h2><span>Music</span> for everyone.</h2>
                            <p>Fill your day with your favorite music! </p>
                         
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="hr-img">
                            <img src="{{ asset('/client/img/hero-bg.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hs-item">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hs-text">
                            <h2><span>Listen </span> to new music.</h2>
                            <p>Fill your day with your favorite music! </p>
                            
                         
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="hr-img">
                            <img src="{{ asset('/client/img/hero-bg.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero section end -->



<div style="background-color: #08192d;">
<section class="blog-section spad" id="playlist">
    <div class="container">
        <div class="row">
 
            @foreach ($lagu as $value)
          
              <div class="col-4 mb-3">
          <div class="card" style="width: 18rem; text-align:center;">
              <img class="card-img-top" src="{{asset('/uploads/img/' . $value->poster)}}" height="150px" width="300px" alt="Card image cap">
              <div class="card-body">
                <h4 class="card-title" font-weight:bold"> {{$value->penyanyi->nama}}  </h4>
                <h6 class="card-title" > {{$value->judul}} </h6>
                <span class="badge badge-pill badge-primary" >Genre: {{$value->genre->nama}}</span>
                <p>
                <p class="card-text">{{Str::limit($value->lirik, 100)}}</p>
                <br>
                <a href="clientview/{{$value->id}}" class="btn btn-primary" >Detail</a>
                <a href="{{$value->link_lagu}}" class="btn btn-danger" >Download</a>
                
              </div>
          </div>
            </div>
@endforeach
    </div>
    </div>
</section>
</div>
@endsection 
