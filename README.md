## Final Project

Kelompok 13

## Anggota Kelompok

<ul>
    <li>Ary Rizma Aulia</li>
    <li>Novia Arifa Ningsih</li>
    <li>Ulfa Hafiza</li>
</ul>

## Tema Project
Website Musik

## ERD

<img src="/public/image/ERD_Final.png" style="text-align:center">

Keterangan :
<ul>
    <li>user : id(INT), nama(VARCHAR), password(VARCHAR)</li>
    <li>profile : id(INT), nama(VARCHAR), bio(TEXT), alamat(TEXT)</li>
    <li>genre : id(INT), nama(VARCHAR)</li>
    <li>penyanyi : id(INT), nama(VARCHAR), umur(INT)</li>
    <li>lagu : id(INT), judul(VARCHAR), tahun(YEAR), lirik(TEXT), poster(VARCHAR), link_lagu(VARCHAR)</li>
</ul>

Relasi :
<ul>
    <li>Satu user hanya dapat memiliki satu profile</li>
    <li>Satu genre memiliki banyak lagu</li>
    <li>Satu penyanyi memiliki banyak lagu</li>
    <li>User dapat memberikan penilaian (rating dan komentar) kepada banyak lagu, begitupun lagu dapat diberikan penilaian (rating dan komentar) oleh banyak user</li>
</ul>

## Link Video
Link Demo Aplikasi : https://drive.google.com/file/d/1qL3Bg2Kwvbqdu3ej6uwWPO9nqZielkqR/view?usp=sharing

Link Deploy : http://laravelmusik.lelangsepatu.store/

NB : Bila link deploy tidak dapat diakses setelah register atau login, atur pada pengaturan browser untuk izinkan 'konten tidak aman'
